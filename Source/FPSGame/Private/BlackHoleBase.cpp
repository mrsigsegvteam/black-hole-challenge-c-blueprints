// Fill out your copyright notice in the Description page of Project Settings.

#include "BlackHoleBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"


// Sets default values
ABlackHoleBase::ABlackHoleBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = MeshComp;

	SphereGravityComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereGravComp"));
	SphereGravityComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereGravityComp->SetupAttachment(MeshComp);

	SphereDestroyComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereDesComp"));
	SphereDestroyComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereDestroyComp->SetupAttachment(MeshComp);

	// Bind to Event
	SphereDestroyComp->OnComponentBeginOverlap.AddDynamic(this, &ABlackHoleBase::OverlapInnerSphere);

}

void ABlackHoleBase::checkNearbyActors()
{
	TArray<UPrimitiveComponent*> OverlapingActorsGravity;
	SphereGravityComp->GetOverlappingComponents(OverlapingActorsGravity);

	for (int32 i = 0; i < OverlapingActorsGravity.Num(); i++)
	{
		UPrimitiveComponent* primitive = OverlapingActorsGravity[i];
		if (primitive && primitive->IsSimulatingPhysics())
		{
			const float SphereRadius = SphereGravityComp->GetScaledSphereRadius();
			const float ForceStrength = -2000; // Negative value to make it pull towards the origin instead of pushing away
			primitive->AddRadialForce(GetActorLocation(), SphereRadius, ForceStrength, ERadialImpulseFalloff::RIF_Constant, true);

		}
	}
}

void ABlackHoleBase::OverlapInnerSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		OtherActor->Destroy();
	}
}

// Called when the game starts or when spawned
void ABlackHoleBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlackHoleBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	checkNearbyActors();

}

